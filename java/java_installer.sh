#!/bin/sh

#execute as root or sudo

echo "[add linuxuprising ppa to /etc/apt/sources.list.d/linuxuprising-java.list]"
echo "deb http://ppa.launchpad.net/linuxuprising/java/ubuntu bionic main" > /etc/apt/sources.list.d/linuxuprising-java.list

echo "[add apt-keys]"
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 73C3DB2A

echo "[update & download oracle-java14-installer]"
apt-get update && apt-get install oracle-java14-installer -y

echo "[cleanup install]"
rm /etc/apt/sources.list.d/linuxuprising-java.list
apt-key remove 73C3DB2A
