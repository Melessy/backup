#!/bin/sh

echo "[Run as root]"

echo "[Installing sudo]"
apt-get install sudo -y

echo "[Added user (melessy) to sudo group]"
usermod -aG sudo melessy