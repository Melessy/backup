#!/bin/bash
#works on debian
#chmod +x this file
#Change VPN_NAME
#Change RECONNECT_TIME
VPN_NAME="T7x"
RECONNECT_TIME=7
while [ "true" ]
do
        VPNCON=$(nmcli con show --active | grep $VPN_NAME | cut -f1 -d " ")
        if [[ $VPNCON != $VPN_NAME ]]; then
                echo "Disconnected, trying to reconnect..."
                (sleep 1s && nmcli con up $VPN_NAME)
        else
                echo "Already connected to VPN: $VPN_NAME"
        fi
        sleep $RECONNECT_TIME
done
