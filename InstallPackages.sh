#!/bin/sh

echo "[Installing conky..]"
sudo apt-get install conky -y

echo "[Installing nautilus..]"
sudo apt-get install nautilus -y

echo "[Installing git..]"
sudo apt-get install git -y

echo "[Installing ufw..]"
sudo apt-get install ufw -y